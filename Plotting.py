

import matplotlib as mp
import matplotlib.pyplot as plt
import numpy as np

from matplotlib.offsetbox import OffsetImage, AnnotationBbox

standard_dashes = ('-', '--', ':')
cm2inch = 0.393701


class Plot(object):
  params = {                      # setup matplotlib to use latex for output
    "pgf.texsystem": "xelatex",        # change this if using xetex or lautex
    "text.usetex": True,                # use LaTeX to write all text
    "font.family": "serif",
    "font.serif": "Libertinus Serif",                   # blank entries should cause plots to inherit fonts from the document
    "font.sans-serif": "Libertinus Sans",
    "font.monospace": [],
    "axes.labelsize": 10,               # LaTeX default is 10pt font.
    "axes.linewidth": 1.0,
    "font.size": 10,
    "legend.fontsize": 10,               # Make the legend/label fonts a little smaller
    "xtick.labelsize": 10,
    "ytick.labelsize": 10,
   }
  figure_size = (7.0*cm2inch, 4.0*cm2inch)
  figure = None
  axes = None
  colors = None
  plots = None
  xticks = None
  barwidth = 0.2
  bar_size = 1.0
  bar_dash = '-'
  line_dashes = '-'
  line_size_factor = 0.4
  alpha = 1.0
  margins = (0.0, 0.0)
  add_artists = None
  add_lines = None
  xlim = None, None
  ylim = None, None
  axes_labels = None, None
  legend_pos = 'upper center'
  do_legend = True

  def __init__(self, states = None, energies = None, labels = None, **kwargs):
    self.plots = []
    for e,l in zip(energies,labels):
      self.plots.append((e, l))
    self.xticks = states
    self.labels = { 'X': kwargs.pop('xlabel', 'State'),
                    'Y': kwargs.pop('ylabel', 'Energy') }
    self.colors = ['black', 'red', 'blue', 'green', 'teal', 'orange', 'purple', 'yellow']
    self.line_dashes = kwargs.pop('linedashes', ['-'] * len(self.plots))

  def _do_Plot(self):
    mp.rcParams.update(mp.rcParamsDefault)
    mp.rcParams.update(self.params)

  def show(self):
    if self.figure is None:
        self._do_Plot()
        plt.tight_layout(pad=0.2)
    plt.show()

  def write_PDF(self, path = None, dpi=300):
    if self.figure is None:
      self._do_Plot()
    plt.tight_layout(pad=0.2)
    plt.savefig(path, dpi=dpi)

  def add_img(self, img = None, xy = None):
    if self.add_artists is None:
        self.add_artists = []
    imagebox = OffsetImage(plt.imread(path), zoom = scale)
    xybox = kwargs.pop('xybox', (120, 120))
    xycoords = kwargs.pop('xycoords', 'data')
    boxcoords = kwargs.pop('boxcoords', 'offset points')
    pad = kwargs.pop('pad', 0.5)
    box = AnnotationBbox(imagebox, xy, xybox = xybox, xycoords = xycoords,
                        boxcoords = boxcoords, pad = pad, zorder = 0)
    self.add_artists.append(box)

  def add_line(self, *args, **kwargs):
    if kwargs.pop('vertical', False):
      func = plt.axvline
    else:
      func = plt.axhline
    if self.add_lines is None:
      self.add_lines = []
    self.add_lines.append((func, args, kwargs))


class States_vs_Energy(Plot):
  axes_labels = None, r'$\Delta G_{298}^\circ$ / kJ\,mol$^{-1}$'

  def _add_xyvals(self, xvals=None, yvals=None, **kwargs):
    raise NotImplementedError('Just what it says.')

  def _set_ticks(self, ax = None):
    xvals = np.arange(len(self.xticks))
    ax.set_xlim(xvals[0]-0.5-self.margins[0], xvals[-1]+0.5+self.margins[1])
    ax.set_xticks(xvals)
    ax.set_xticklabels(self.xticks)

  def _do_Plot(self):
    super()._do_Plot()
    self.figure = plt.figure(figsize=self.figure_size)
    ax1 = self.figure.add_subplot(111)
    self._set_ticks(ax1)
    if self.axes_labels[0] is not None:
      plt.xlabel(self.axes_labels[0])
    if self.axes_labels[1] is not None:
      plt.ylabel(self.axes_labels[1])
    self.axes = [ax1]
    legend_plots = []
    legend_texts = []
    plot_num = len(self.plots)
    for idx, (xyval, lbl) in enumerate(self.plots):
      pline = self._add_xyvals(xyval[0], xyval[1], iplot=idx, nplots=plot_num)
      legend_plots.append(pline)
      legend_texts.append(lbl)
    if self.add_artists:
      for a in self.add_artists:
        ax1.add(a)
    if self.add_lines:
      for func, args, kwargs in self.add_lines:
        txt = kwargs.pop('label', None)
        line = func(*args, **kwargs)
        if None not in (line, txt):
          legend_plots.append(line)
          legend_texts.append(txt)
    if self.do_legend:
      ax1.legend(legend_plots, legend_texts, loc=self.legend_pos, frameon=False)

class Standard_Lines(Plot):

  def __init__(self, xyvals=None, plot_names=None, **kwargs):
    xmin = min(x[0] for x,y in xyvals)
    xmax = max(x[-1] for x,y in xyvals)
    step = kwargs.pop('xsteps', 2)
    self.xticks = np.arange(xmin, xmax+1, step=step)
    self.plots = list(zip(xyvals, plot_names))
    label_xaxis = kwargs.pop('xlabel', 'Amount')
    label_yaxis = kwargs.pop('ylabel', 'Energy')
    self.axes_labels = (label_xaxis, label_yaxis)
    
    self.line_dashes = kwargs.pop('dashes', ['-']*len(self.plots))
    self.colors = kwargs.pop('colors', ['black', 'red', 'green', 'magenta'])
    self.legend_pos = kwargs.pop('legend_position', 'upper right')
  
  def _set_ticks(self, ax = None):
    xvals = self.xticks
    func_lim = ax.set_xlim
    func_ticks = ax.set_xticks
    if None in self.xlim:
      func_lim(xvals[0]-0.5-self.margins[0], xvals[-1]+0.5+self.margins[1])
    else:
      func_lim(*self.xlim)
    if None not in self.ylim:
      ax.set_ylim(*self.ylim)

  def _do_Plot(self):
    super()._do_Plot()
    self.figure = plt.figure(figsize=self.figure_size)
    ax1 = self.figure.add_subplot(111)
    self._set_ticks(ax1)
    plt.xlabel(self.axes_labels[0])
    plt.ylabel(self.axes_labels[1])
    self.axes = [ax1]
    legend_plots = []
    legend_texts = []
    plot_num = len(self.plots)
    for idx, (xyval, lbl) in enumerate(self.plots):
      ax1.plot(xyval[0], xyval[1], self.line_dashes[idx], color=self.colors[idx],
               linewidth=self.bar_size, alpha=self.alpha, label=lbl)
      #legend_plots.append(pline)
      #legend_texts.append(lbl)
    if self.add_lines:
      for func, args, kwargs in self.add_lines:
        txt = kwargs.pop('label', None)
        line = func(*args, **kwargs)
        if None not in (line, txt):
          legend_plots.append(line)
          legend_texts.append(txt)
    ax1.legend(loc=self.legend_pos, frameon=False)

class Energy_Profile(States_vs_Energy):
  
  def _add_xyvals(self, xvals=None, yvals=None, **kwargs):
    old_xy = None
    plot_color = self.colors.pop(0)
    ldash = self.line_dashes.pop(0)
    for x,y in zip(xvals, yvals):
      if old_xy is not None:
          self.axes[0].plot((old_xy[0]+self.barwidth, x-self.barwidth), (old_xy[1], y), ldash,
                   color=plot_color, linewidth=self.line_size_factor*self.bar_size,
                   alpha=self.alpha)
      pline = plt.hlines(y, x-self.barwidth, x+self.barwidth, colors=plot_color,
                         linestyles=ldash, #self.bar_dash,
                         linewidths=self.bar_size,
                         alpha=self.alpha)
      old_xy = (x, y)
    return pline

class ES_Bars(States_vs_Energy):
  horizontal = False

  def _add_xyvals(self, xvals=None, yvals=None, **kwargs):
    iplot = kwargs.pop('iplot', 0)
    nplots = kwargs.pop('nplots', 1)
    plot_color = self.colors.pop(0)
    if not self.horizontal:
      plotfunc = self.axes[0].bar
    else:
      plotfunc = self.axes[0].barh
    print(xvals, yvals)
    bline = plotfunc(xvals+(iplot-nplots/2.0)*self.barwidth, yvals,
                     self.barwidth, color=plot_color,
                     linestyle=self.bar_dash, linewidth=self.bar_size,
                     alpha=self.alpha)
    return bline

  def _set_ticks(self, ax = None):
    xvals = np.arange(len(self.xticks))
    if not self.horizontal:
      func_lim = ax.set_xlim
      func_ticks = ax.set_xticks
      func_labels = ax.set_xticklabels
    else:
      func_lim = ax.set_ylim
      func_ticks = ax.set_yticks
      func_labels = ax.set_yticklabels
      self.axes_labels = self.axes_labels[::-1]
    if None in self.xlim:
      func_lim(xvals[0]-0.5-self.margins[0], xvals[-1]+0.5+self.margins[1])
    else:
      func_lim(*self.xlim)
    func_ticks(xvals)
    func_labels(self.xticks)

