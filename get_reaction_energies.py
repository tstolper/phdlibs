#!/usr/bin/env python

import argparse

from do_ASM import Calculation
from thermo import RRHO

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Extract energies/thermodynamic corrections from Orca(3) output files.')
  parser.add_argument('--educt', nargs=1, help='')
  parser.add_argument('--product', action='append', nargs=1, help='')
  args = parser.parse_args()
  out_parse = Calculation()
  products = []
  educts = []

  print('{:15s} {:12s} {:12s} {:12s} | {:12s}'.format('File', 'SCF(opt)/Eh', 'G-Eel/kJ/mol', 'SCF(HL)/Eh', 'G+HL/Eh'))
  for p in args.educt:
    vibout, scfout, sn = p.split(',')
    freq_out = out_parse.parse_output(vibout)
    therm = RRHO(freq_out['geoms'][-1], freq_out['frequencies'][-1], freq_out['multi'])
    escf = freq_out['scf_energies'][-1]
    G = therm.get_G(p = 1.01325e+5, T = 298.15, sigma = float(sn), vib_above = 35.0)/1000.0
    elen_out = out_parse.parse_output(scfout)
    HL_SCF = elen_out['scf_energies'][-1]
    print('{:15s} {:12.8f} {:12.8f} {:12.8f} | {:12.8f}'.format(
           scfout, escf, G, HL_SCF, G/2625.5e+3 + HL_SCF))
    educts.append(G + HL_SCF*2625.5)
  
  for p in args.product:
    vibout, scfout, sn = p[0].split(',')
    freq_out = out_parse.parse_output(vibout)
    therm = RRHO(freq_out['geoms'][-1], freq_out['frequencies'][-1], freq_out['multi'])
    escf = freq_out['scf_energies'][-1]
    G = therm.get_G(p = 1.01325e+5, T = 298.15, sigma = float(sn), vib_above = 35.0)/1000.0
    elen_out = out_parse.parse_output(scfout)
    HL_SCF = elen_out['scf_energies'][-1]
    print('{:15s} {:12.8f} {:12.8f} {:12.8f} | {:12.8f}'.format(
           scfout, escf, G, HL_SCF, G/2625.5e+3 + HL_SCF))
    products.append(G + HL_SCF*2625.5)
    
  print('Reaction energy: {:12.8f} kJ/mol'.format(sum(products)-sum(educts)))
  
