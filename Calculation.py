

import numpy as np
from .Constants import element_masses

class Trajectory(object):
  geometries = None
  steps = None
  
  def __init__(self, geomlist=None):
    if geomlist is not None and len(geomlist) > 0:
      self.geometries = geomlist
      self.steps = len(geomlist)
  
  def __getitem__(self, index=None):
    if index is not None:
      if index < self.steps:
        return self.geometries[index]
      else:
        raise IndexError('Index out of bounds. Length: {:5d} Index: {:5d}'.format(self.steps, index))
    else:
      raise IndexError('No index? O_o')

  def __setitem__(self, item=None, index=None):
    if None not in (item, index):
      if isinstance(item, Geometry):
        if index < self.steps:
          self.geometries[i] = item
        else:
          raise IndexError('Index out of bounds. Length: {:5d} Index: {:5d}'.format(self.steps, index))
      else:
        raise TypeError('Expected Geometry, got ' + type(item))
    else:
      raise ValueError('Forgot to set something? item={:s}, index={:d}'.format(repr(item), index))

  def __contains__(self, geom=None):
    if isinstance(geom, Geometry):
      return geom in self.geometries
    else:
      raise TypeError('Expected Geometry, got ' + type(item))


class Trajectory_File(Trajectory):
  def __init__(self, fpath=None):
    geoms = self.read_trj(fpath)
    super().__init__(geoms)
  
  def read_trj(self, fpath=None):
    with open(fpath, 'r') as tfl:
      geoms = []
      lines = tfl.readlines()
      natoms= int(lines[0])
      for sid in range(len(lines)//(natoms+2)):
        xyzstr = ''.join(lines[sid*(natoms+2):(sid+1)*(natoms+2)])
        geoms.append(Geometry(xyzstr))
    return geoms
      
  def save_trj(self, fpath=None):
    raise NotImplementedError('Uiuiui.')


class Geometry(object):
  size = None
  xyz = None
  atoms = None
  atom_labels = None
  _mass = None
  _mass_center = None

  def __init__(self, fpath=None):
    if fpath:
      self.read_xyz(fpath)

  def __eq__(self, other):
    is_same_structure = True
    if not isinstance(other, Geometry):
      raise TypeError('ERROR: Cannot compare different objects (yet).')
    if self.size != other.size:
      is_same_structure = False
    elif self.atoms != other.atoms:
      is_same_structure = False
    elif not np.allclose(self.xyz, other.xyz):
      is_same_structure = False
    return is_same_structure

  def __str__(self):
    res = '{:5d}\n\n'.format(self.size)
    res += '\n'.join('{:5s} {:12.6f} {:12.6f} {:12.6f}'.format(a,
                      *v) for a, v in zip(self.atoms, self.xyz))
    return res
  
  def _read_stream(self, stream=None):
    pos = []
    atms= []
    for line in stream:
      ls = line.split()
      if len(ls) >= 4:
        try:
          a = ls[0].capitalize()
          p = [ float(f) for f in ls[1:4] ]
          atms.append(a)
          pos.append(p)
        except ValueError as err:
          raise err
    if len(atms) != len(pos):
      raise ValueError('ERROR: Labels and coordinates do not align!')
    else:
      return len(atms), atms, np.array(pos)

  def read_xyz(self, fpath=None):
    try:
      strm = fpath.split('\n')
      if len(strm) > 2:
        natoms, atoms, pos = self._read_stream(strm[2:])
      elif len(strm) == 1:
        with open(strm[0], 'r') as fl:
          natoms, atoms, pos = self._read_stream(fl)
      else:
        raise Exception('ERROR: weird path/xyz? ' + fpath)
    except ValueError as err:
      print('WARNING: Possible geometry conversion problem' +\
            ' in {:s}: {:s}'.format(fpath, err))
    self.xyz = pos
    self.atoms = atoms
    self.size = natoms

  def get_subgeom(self, ids=None):
    if ids is not None:
      res = '{:5d}\n\n'.format(len(ids))
      res += '\n'.join('{:5s} {:12.6f} {:12.6f} {:12.6f}'.format(a,
                      *v) for i, a, v in zip(range(self.size),
                      self.atoms, self.xyz) if i in ids)
      return Geometry(res)

  def get_fragments(self, *args):
    res = []
    for f in args:
      res.append(self.get_subgeom(f))
    return res

  @property
  def mass(self):
    if self._mass is None:
      self._mass =  np.sum([element_masses[elem] for elem in self.atoms])
    return self._mass

  @property
  def mass_center(self):
    if self._mass_center is None:
      mass_vec = np.array([element_masses[elem] for elem in self.atoms])
      self._mass_center = np.sum(np.dot(np.diag(mass_vec), self.xyz), axis=0)/self.mass
    return self._mass_center

  def get_Inertia(self):
    inert_mat = np.zeros((3,3))
    body_coords = self.xyz - self.mass_center
    for elem, atom in zip(self.atoms, body_coords):
      mass = element_masses[elem]
      # diagonals
      inert_mat[0,0] += mass*(atom[1]**2 + atom[2]**2)
      inert_mat[1,1] += mass*(atom[0]**2 + atom[2]**2)
      inert_mat[2,2] += mass*(atom[0]**2 + atom[1]**2)
      # off-diagonals
      inert_mat[0,1] -= mass*atom[0]*atom[1]
      inert_mat[0,2] -= mass*atom[0]*atom[2]
      inert_mat[1,2] -= mass*atom[1]*atom[2]
    inert_mat[1,0] = inert_mat[0,1]
    inert_mat[2,0] = inert_mat[0,2]
    inert_mat[2,1] = inert_mat[1,2]
    val, vec = np.linalg.eigh(inert_mat)
    return val, vec

class Orca3(object):
  template = None
  input_path = None
  results = None
  geometry = None
  charge = None
  spin = None
  
  def __init__(self, fpath=None):
    self.input_path = fpath

  def read_input(self, fpath=None):
    read_xyz = False
    geom_str = '{:5d}\n\n'
    atoms = 0
    with open(fpath, 'r') as fl:
      for line in fl:
        ls = line.split()
        if ls[0][0] == '*' and 'xyz' in line:
          self.charge = int(ls[-2])
          self.spin = int(ls[-1])
          read_xyz = True
        elif ls[0][0] == '*' and read_xyz:
          read_xyz = False
          geom_str.format(atoms)
          self.geometry = Geometry(geom_str)
        elif read_xyz:
          if len(ls) >= 4:
            geom_str += line
            atoms += 1

  def write_input(self, fpath=None):
    with open(fpath, 'w') as fl:
      fl.write(template.format(charge=self.charge,
               spin_state=self.spin_state, geometry=str(self.geometry)) + '\n')
  
  def parse_hessian(fpath = None):
    with open(fpath, 'r') as fl:
      hess = None
      idx, row, cols = 0, None, None
      natoms = None
      parse = False
      parse_type = None
      for line in fl:
        if parse:
          if line[0] == '$':
            parse_type = 'none'
            parse = False
          elif parse_type == 'hessian':
            ls = line.split()
            if len(ls) > 1:
              if idx < natoms*3:
                row = int(ls[0])
                idx += 1
                for i, col in enumerate(cols):
                  hess[row, col] = float(ls[i+1])
              else:
                cols = [ int(v) for v in ls ]
                idx = 0

        if '$hessian' in line:
          natoms = int(next(fl))//3
          __ = next(fl)
          cols = [ int(v) for v in __.split() ]
          parse = True
          parse_type = 'hessian'
          hess = np.zeros((natoms*3, natoms*3))
    return hess

  def parse_output(self, fpath=None):
    with open(fpath, 'r') as ofl:
      calc_vals = {}
      for line in ofl:
        if 'CARTESIAN COORDINATES (ANGSTROEM)' in line:
          next(ofl)
          xyz_str = '{:5d}\n\n'
          natoms = 0
          for row in ofl:
            if len(row) > 8:
              xyz_str += row
              natoms += 1
            else:
              break
          xyz_str.format(natoms)
          if 'geoms' not in calc_vals:
            calc_vals['geoms'] = []
          calc_vals['geoms'].append(Geometry(xyz_str))
          if 'natoms' not in calc_vals:
            calc_vals['natoms'] = natoms
        elif '* Single Point Calculation *' in line:
          calc_vals['run_type'] = 'SCF'
        elif 'Total Charge           Charge          ....' in line:
          calc_vals['charge'] = int(line.split()[-1])
        elif 'Multiplicity           Mult            ....' in line:
          calc_vals['multi'] = int(line.split()[-1])
        elif 'FINAL SINGLE POINT ENERGY' in line:
          if 'final_energies' not in calc_vals:
            calc_vals['final_energies'] = []
          calc_vals['final_energies'].append(float(line.split()[-1]))
        elif 'Dispersion correction' in line:
          if 'disp_energies' not in calc_vals:
            calc_vals['disp_energies'] = []
          lsplit = line.split()
          if lsplit[-1][0] == '-':
            calc_vals['disp_energies'].append(float(lsplit[-1]))
        elif 'TOTAL RUN TIME' in line:
          calc_vals['no_crash'] = True
        elif 'UHF SPIN CONTAMINATION' in line:
          if 'uhf_spin_devs' not in calc_vals:
            calc_vals['uhf_spin_devs'] = []
          [ next(ofl) for i in range(8) ]
          calc_vals['uhf_spin_devs'].append(float(next(ofl).split()[-1]))
        elif 'ORCA NUMERICAL FREQUENCIES' in line:
          # numerical frequencies -> handle as block?
          # what about analytical?
          pass
        elif 'VIBRATIONAL FREQUENCIES' in line:
          if 'frequencies' not in calc_vals:
            calc_vals['frequencies'] = []
          next(ofl)
          tmpvibs = []
          for row in ofl:
            if '------------' in row:
              break
            else:
              rs = row.split()
            if len(rs) >= 3:
              tmpvibs.append(float(rs[1]))
          calc_vals['frequencies'].append(np.array(tmpvibs))
          assert calc_vals['natoms']*3 == len(calc_vals['frequencies'][-1]), 'Did not read 3N vibrations? natoms={:d}, vibs={:d}'.format(
                 calc_vals['natoms'], len(calc_vals['frequencies'][-1]))
        elif 'Temperature' in line:
          calc_vals['temperature'] = float(line.split()[2])
        elif 'Pressure' in line:
          calc_vals['pressure'] = float(line.split()[2])
        elif 'Total Mass' in line:
          calc_vals['total mass'] = float(line.split()[3])
        elif 'THERMOCHEMISTRY AT' in line:
          # start of thermochem -> also block?
          if 'thermo' not in calc_vals:
            calc_vals['thermo'] = []
          calc_vals['thermo'].append({})
        elif 'Zero point energy' in line:
          calc_vals['thermo'][-1]['ZPE'] = float(line.split()[4])
        elif 'Thermal vibrational correction' in line:
          calc_vals['thermo'][-1]['Hvib'] = float(line.split()[4])
        elif 'Thermal rotational correction' in line:
          calc_vals['thermo'][-1]['Hrot'] = float(line.split()[4])
        elif 'Thermal translational correction' in line:
          calc_vals['thermo'][-1]['Htr'] = float(line.split()[4])
        elif 'Electronic entropy' in line:
          calc_vals['thermo'][-1]['Sel'] = float(line.split()[3])
        elif 'Vibrational entropy' in line:
          calc_vals['thermo'][-1]['Svib'] = float(line.split()[3])
        elif 'Rotational entropy' in line:
          calc_vals['thermo'][-1]['Srot'] = float(line.split()[3])
        elif 'Translational entropy' in line:
          calc_vals['thermo'][-1]['Str'] = float(line.split()[3])
    return calc_vals

  def check_output(self, fpath=None):
    is_same_calc = False
    did_run = False

    results = self.parse_output(fpath)
    if self.geometry == results['geoms'][0]:
      if self.charge == results['charge'] and self.spin != results['multi']:
        is_same_calc = True
    if len(results['scf_energies']) > 0:
      did_run = True
    return is_same_calc and did_run

  def run(): pass


class MolproDev(Orca3):

  def __init__(self, fpath = None):
    self.input_path = fpath

  def parse_output(self, fpath=None):
    with open(fpath, 'r') as ofl:
      calc_vals = {}
      for line in ofl:
        if 'ATOMIC COORDINATES' in line:
          [ next(ofl) for i in range(3) ]
          xyz_str = '{:5d}\n\n'
          natoms = 0
          for row in ofl:
            ls = row.split()
            if len(ls) == 6:
              xyz_str += '{:5s} {:12.6f} {:12.6f} {:12.6f}\n'.format(ls[1], float(ls[3]),
                                                            float(ls[4]), float(ls[5]))
              natoms += 1
            else:
              break
          xyz_str.format(natoms)
          if 'geoms' not in calc_vals:
            calc_vals['geoms'] = []
          calc_vals['geoms'].append(Geometry(xyz_str))
          if 'natoms' not in calc_vals:
            calc_vals['natoms'] = natoms
        elif 'PROGRAM * RHF-SCF (CLOSED SHELL)' in line:
          if 'RHF' not in calc_vals:
            calc_vals['RHF'] = []
          rhf = { 'final energy': None }
          for row in ofl:
            if 'CONVERGENCE THRESHOLDS:' in row:
              rs = row.split()
              rhf['conv thr dens'] = float(rs[2])
              rhf['conv thr ener'] = float(rs[4])
            if 'ITERATION    DDIFF          GRAD' in row: # SCF
              scf = []
              for scfl in ofl:
                s = scfl.split()
                if len(s) > 0:
                  scf.append([ float(v.replace('D', 'E')) for v in s[1:-1] ])
                else:
                  break
              rhf['scf'] = np.array(scf)
            if '!RHF STATE 1.1 Energy' in row:
              rhf['final energy'] = float(row.split()[4])
            if 'LUMO-HOMO' in row:
              break
          calc_vals['RHF'].append(rhf)
        elif 'PROGRAM * OPT (Geometry optimization)' in line:
          opt = None
          read_opt = False
          for row in ofl:
            if 'ITER.   ENERGY(OLD)    ENERGY(NEW)      DE          GRADMAX' in row:
              opt = { 'energies': [], 'gradmax': [], 'gradnorm': [], 'gradrms': [],
                      'stepmax': [], 'steplen': [], 'steprms': [], 'time': []}
              read_opt = True
            elif read_opt:
              ls = row.split()
              if len(ls) > 0:
                opt['energies'].append(float(ls[2]))
                opt['gradmax'].append(float(ls[4]))
                opt['gradnorm'].append(float(ls[5]))
                opt['gradrms'].append(float(ls[6]))
                opt['stepmax'].append(float(ls[7]))
                opt['steplen'].append(float(ls[8]))
                opt['steprms'].append(float(ls[9]))
                opt['time'].append(float(ls[10]))
              else:
                break
          calc_vals['optimization'] = opt
        elif 'PROGRAM * FREQUENCIES' in line:
          if 'frequencies' not in calc_vals:
            calc_vals['frequencies'] = []
          vib = []
          for fl in ofl:
            if 'Imaginary Vibration' in fl:
              next(ofl)
              for row in ofl:
                rs = row.split()
                if len(rs) > 0:
                  vib.append(-float(rs[1]))
                else:
                  break
            elif 'Vibration' in fl:
              next(ofl)
              for row in ofl:
                rs = row.split()
                if len(rs) > 0:
                  vib.append(float(rs[1]))
                else:
                  break
            elif 'Zero point energy:' in fl:
              if 'thermo' not in calc_vals:
                calc_vals['thermo'] = []
              calc_vals['thermo'].append({})
              ls = fl.split()
              calc_vals['thermo'][-1]['ZPE'] = float(ls[3])
              break # break FREQUENCIES program
          calc_vals['frequencies'].append(np.array(vib))
        elif 'DATASETS' in line:
          for row in ofl:
            if 'CPU TIMES' in row:
              if 'timing' not in calc_vals:
                calc_vals['timing'] = { 'cpu total': [], 'real total': [] }
              ls = row.split()
              calc_vals['timing']['cpu total'].append(float(ls[3]))
            elif 'REAL TIME' in row:
              ls = row.split()
              calc_vals['timing']['real total'].append(float(ls[3]))
            elif '**********************' in row:
              break
    return calc_vals

