#!/usr/bin/env python

import warnings
from .Calculation import Orca3
from .thermo import RRHO
from .Constants import R, Eh2kJmol, cal2J

class Protocol(object):
  def __init__(self, educts=None, products=None):
    self.educts = educts
    self.products = products
    self.additions = { 'educts': [], 'products': [] }

  def add_water(self, product=False):
    key = 'educts'
    if product:
      key = 'products'
    self.additions[key].extend(self._water_corrections)

  def add_proton(self, product=False):
    key = 'educts'
    if product:
      key = 'products'
    self.additions[key].extend(self._proton_corrections)

  def add_electron(self, product=False):
    key = 'educts'
    if product:
      key = 'products'
    self.additions[key].extend(self._electron_corrections)

  def dEopt(self): pass
  
  def dEsp(self): return NotImplemented

  def dEextra(self): return NotImplemented

  def dH(self): pass

  def dS(self): pass

  def dG(self): pass

  def dGsolv(self): pass

  def dGproto(self): return NotImplemented


class CCB_OrcaThermo(Protocol):
  pressure = 1.01325e+5 # Pa
  temperature = 298.15 # K
  _dEopt = None
  _dH = None
  _dS = None
  _dG = None
  _dEsp = None
  _dEextra = None
  _dG_proto = None
  vib_params = { 'vib_start': 6,
                 'vib_swap': None,
                 'vib_above': 35.0
               }
  
  @property
  def dEopt(self):
    if self._dEopt is None:
      res = 0.0
      for p in self.products:
        res += p.get_Eel_opt()
      for e in self.educts:
        res -= e.get_Eel_opt()
      self._dEopt = res*Eh2kJmol
    return self._dEopt

  @property
  def dH(self):
    if self._dH is None:
      p, T = self.pressure, self.temperature
      res = 0.0
      for sign, mol in zip([1.0]*len(self.products) + [-1.0]*len(self.educts),
                         self.products + self.educts):
        t = RRHO(mol.get_geom(), mol.get_vibs(), mol.get_multi())
        h = t.get_H_trans(p, T)
        h += t.get_H_rot(p, T)
        h += t.get_H_vib(p, T, **self.vib_params)
        h += t.get_H_ZPE(p, T, **self.vib_params)
        res += (h + R*T)*sign
      self._dH = self.dEopt + res/1000.0
    return self._dH

  @property
  def dS(self):
    if self._dS is None:
      p, T = self.pressure, self.temperature
      res = 0.0
      for sign, mol in zip([1.0]*len(self.products) + [-1.0]*len(self.educts),
                         self.products + self.educts):
        t = RRHO(mol.get_geom(), mol.get_vibs(), mol.get_multi())
        s = t.get_S_trans(p, T)
        s += t.get_S_rot(p, T, sigma=mol.sym_num)
        s += t.get_S_vib(p, T, **self.vib_params)
        s += t.get_S_el(p, T)
        res += s*sign
      self._dS = res/1000.0
    return self._dS

  @property
  def dG(self):
    if self._dG is None:
      self._dG = self.dH - self.temperature*self.dS
    return self._dG

  @property
  def dGsolv(self):
    if self._dGsolv is None:
      pass
    return self._dGsolv

  @property
  def dEsp(self):
    if self._dEsp is None:
      res = 0.0
      for sign, p in zip([1.0]*len(self.products) + [-1.0]*len(self.educts),
                         self.products + self.educts):
        res += sign*p.get_Eel_HL()
      self._dEsp = res*Eh2kJmol
    return self._dEsp

  @property
  def dEextra(self):
    if self._dEextra is None:
      res = 0.0
      for sign, p in zip([1.0]*len(self.products) + [-1.0]*len(self.educts),
                         self.products + self.educts):
        res += sign*p.get_Extra()
      self._dEextra = res*Eh2kJmol
    return self._dEextra

  @property
  def dG_proto(self):
    if self._dG_proto is None:
      self._dG_proto = (self.dEsp + self.dEextra) + (self.dG - self.dEopt)
    return self._dG_proto


class Cramer_Thermo(CCB_OrcaThermo):
  _1atm_to_1M = 1.9*cal2J
  _1atm_to_556M = 4.3*cal2J
  _H2O_1M1M_solv_exp = -6.3*cal2J
  _Hplus_1M1M_solv_exp = -265.9*cal2J
  _Hplus_Gstd = -26.27 # IC-B
  _eminus_Gstd = -0.05 # IC-B
  _NHE_pot = -4.28
  _water_corrections = (_1atm_to_556M,
                        _H2O_1M1M_solv_exp)
  _proton_corrections = (_1atm_to_1M,
                         _Hplus_1M1M_solv_exp)
  _electron_corrections = None
  vib_params = { 'vib_start': 6,
                 'vib_swap': 50.0,
                 'vib_above': 0.0
               }
  
  @property
  def dG_proto(self):
    if self._dG_proto is None:
      super().dG_proto
      self._dG_proto += sum(self.additions['products'])
      self._dG_proto -= sum(self.additions['educts'])
    return self._dG_proto


class Siegbahn_Thermo(Cramer_Thermo):
  _Hplus_1M1M_solv_exp = -264.0*cal2J
  _NHE_pot = 4.281
  _Rubpy_pot = 1.28
  _Rubpy_el_affinity = 127.8*cal2J
  _pH = 7.2
  _proton_corrections = (Cramer_Thermo._Hplus_Gstd,
                         Cramer_Thermo._1atm_to_1M,
                         _Hplus_1M1M_solv_exp,
                         -R*CCB_OrcaThermo.temperature*_pH*2.303/1000.0)
  _electron_corrections = (Cramer_Thermo._eminus_Gstd,
                           Cramer_Thermo._1atm_to_1M,
                           _Rubpy_el_affinity)
  vib_params = { 'vib_start': 6,
                 'vib_swap':  None,
                 'vib_above': 35.0
               }


class Molecule(object):
  outputs = None
  sym_num = 1.0

  def __init__(self, opt=None, sp=None, freq=None, extra=None):
    self.outputs = { }
    if opt is not None:
      out = Orca3().parse_output(opt)
      self.outputs['OPT'] = out
    
    if freq is None:
      if opt is not None:
        self.outputs['FREQ'] = out
    else:
      self.outputs['FREQ'] = Orca3().parse_output(freq)
      if opt is None:
        self.outputs['OPT'] = self.outputs['FREQ']
    
    if sp is not None:
      out = Orca3().parse_output(sp)
      self.outputs['SP'] = out

    if extra is not None:
      if 'EXTRA' not in self.outputs:
        self.outputs['EXTRA'] = {}
      for key,val in extra.items():
        out = Orca3().parse_output(key)
        self.outputs['EXTRA'][key] = (out, val)

  def get_Eel_opt(self):
    if 'OPT' in self.outputs:
      return self.outputs['OPT']['final_energies'][-1]
    else:
      raise ValueError('No optimization output loaded!')

  def get_vibs(self, *args, **kwargs):
    if 'FREQ' in self.outputs:
      return self.outputs['FREQ']['frequencies'][-1]
    else:
      raise ValueError('No frequency output loaded!')

  def get_geom(self):
    if 'OPT' in self.outputs:
      return self.outputs['OPT']['geoms'][-1]
    else:
      raise ValueError('No optimization output loaded!')

  def get_multi(self):
    if 'OPT' in self.outputs:
      return self.outputs['OPT']['multi']
    elif 'FREQ' in self.outputs:
      return self.outputs['FREQ']['multi']
    elif 'SP' in self.outputs:
      return self.outputs['SP']['multi']

  def get_Eel_HL(self):
    if 'SP' in self.outputs:
      return self.outputs['SP']['final_energies'][-1]
    else:
      raise ValueError('No high-level single point output loaded!')

  def get_Extra(self):
    res = 0.0
    if 'EXTRA' in self.outputs:
      for outp, vals in self.outputs['EXTRA'].items():
        for v in vals[1]:
          res += vals[0][v][-1] # TODO: make index selectable!
    else:
      warnings.warn('No extra output loaded! Returning 0.0 as contribution.')
    return res

