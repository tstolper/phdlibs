#!/usr/bin/env python

import numpy as np
from Constants import element_masses
from Calculation import Orca3

class Pathway(object):
  forward = None
  backward = None
  trans_state = None
  
  def get_geoms_around_TS(self): pass


class ASM(Pathway):
  def __init__(self): pass


if __name__ == '__main__':
  import sys
  #bla = Trajectory_File(sys.argv[1])
  #print('Steps: ' + str(bla.steps))
  #####  Parsing Test ############
  frqtest = Orca3()
  res = frqtest.parse_output(sys.argv[1])
  #print('Testing frequency calc parsing.')
  #print('Vibs:')
  #for i,v in enumerate(res['frequencies'][0]):
  #  print('vib {:5d} {:8.2f} cm^-1'.format(i, v))
  #print('Thermodynamic corrections:')
  #for k,v in res['thermo'][0].items():
  #  print('{:10s} {:12.6f}'.format(k, v))
  import thermo
  t = thermo.RRHO(res['geoms'][-1], res['frequencies'][-1], res['multi'])
  print('Htrans:   {:12.8f} Eh'.format(t.get_H_trans()/1000.0/2625.5))
  print('Hrot:     {:12.8f} Eh'.format(t.get_H_rot()/1000.0/2625.5))
  print('ZPE:      {:12.8f} Eh'.format(t.get_H_ZPE()/1000.0/2625.5))
  print('Hvib:     {:12.8f} Eh'.format(t.get_H_vib()/1000.0/2625.5))
  print('T*Strans: {:12.8f} Eh'.format(298.15*t.get_S_trans()/1000.0/2625.5))
  print('T*Srot:   {:12.8f} Eh'.format(298.15*t.get_S_rot()/1000.0/2625.5))
  print('T*Svib:   {:12.8f} Eh'.format(298.15*t.get_S_vib()/1000.0/2625.5))
  print('T*Sel:    {:12.8f} Eh'.format(298.15*t.get_S_el()/1000.0/2625.5))
  print('G: {:16.8f} kcal/mol'.format(t.get_G(sigma=3.0)/1000.0/4.184))

