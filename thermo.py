#!/usr/bin/env python
import warnings
import numpy as np
from .Constants import R, kB, h, c, NA, amu

std_p = 1.01325e+5 # Pa == 1 atm
std_T = 298.15 # K

class RRHO(object):
  frequencies = None
  geometry = None
  multi = None

  def __init__(self, geom=None, freqs=None, multi=None):
    self.multi = multi
    self.frequencies = freqs
    self.geometry = geom

  def _get_vibs(self, vib_swap=None, vib_start=6, vib_above=None, func=None):
    """ Extracts the frequencies for thermodynamic corrections
        and returns a generator.
        Selection can be done in several ways (in that order):
        1.) By a self-defined python function 'func(vib)'.
            Should return the actual frequency in cm^-1 or
            None if it should be excluded.
        2.) By simple parameters (all are checked at the
            same time):
          a) 'vib_swap': if not None every frequency
             below 'vib_swap' is set to 'vib_swap'.
             (lower bound)
          b) 'vib_start': the first 'vib_start' frequencies
             are neglected.
          c) 'vib_above': only returns frequencies which are
             larger or equal to 'vib_above'.
    """
    if np.absolute(self.frequencies[4]) > 1.0e-2:
      warnings.warn('At least the first 5 frequencies should be 0.')
    elif self.frequencies[6] < 0.0:
      warnings.warn('Doing transition state.')
    elif self.frequencies[5] > 1.0:
      warnings.warn('Maybe unconverged minimum.')
    elif vib_swap is not None and self.frequencies[6] < vib_swap:
      warnings.warn('Low vibrations! Swapping them with 50 cm^-1.')
    for i, vib in enumerate(self.frequencies):
      if func is not None:
        v = func(vib)
        if v is None:
          continue
      elif vib_start is None or i >= vib_start:
        if vib_above is None or vib >= vib_above:
          if vib_swap is None or vib > vib_swap:
            v = vib
          else:
            v = vib_swap
        else:
          continue
      else:
        continue
      yield v

  def get_H_trans(self, p=std_p, T=std_T, **kwargs):
    return 3.0*R*T/2.0

  def get_H_rot(self, p=std_p, T=std_T, **kwargs):
    return 3.0*R*T/2.0

  def get_H_vib(self, p=std_p, T=std_T, **kwargs):
    vibs = np.array([vib for vib in self._get_vibs(**kwargs)])
    fac = h*c/kB
    div = np.exp(fac*vibs/T)-1.0
    return R*np.sum(np.divide(fac*vibs, div))

  def get_H_ZPE(self, *args, **kwargs):
    return R*sum(h*c*vib/(2.0*kB) for vib in self._get_vibs(**kwargs))

  def get_S_trans(self, p=std_p, T=std_T, **kwargs):
    qtr = kB*T*np.power(2.0*np.pi*self.geometry.mass*amu*kB*T/h**2, 3.0/2.0)/p
    return R*(5.0/2.0 + np.log(qtr))

  def get_S_rot(self, p=std_p, T=std_T, sigma=1.0, **kwargs):
    Is, Ivecs = self.geometry.get_Inertia()
    iconvert = amu*1.0e-20
    Is *= iconvert
    qrot = np.sqrt(np.pi*Is[0])*np.sqrt(Is[1])*np.sqrt(Is[2])*np.power(8.0*(np.pi**2)*kB*T/(h**2), 1.5)
    return R*(3.0/2.0 + np.log(qrot/sigma))

  def get_S_vib(self, p=std_p, T=std_T, **kwargs):
    vibs = np.array([vib for vib in self._get_vibs(**kwargs)])
    qvib = np.exp(h*c*vibs/(kB*T))
    term = h*c*vibs/(kB*T*(qvib-1.0)) - np.log((qvib-1.0)/qvib)
    return R*np.sum(term)

  def get_S_el(self, *args, **kwargs):
    return R*np.log(self.multi)

  def get_G(self, p=std_p, T=std_T, sigma=1.0, **kwargs):
    Hsum = self.get_H_trans(p,T) + self.get_H_rot(p,T) + self.get_H_vib(p,T,**kwargs) + self.get_H_ZPE(p,T,**kwargs)
    Ssum = self.get_S_trans(p,T) + self.get_S_rot(p,T,sigma) + self.get_S_vib(p,T,**kwargs) + self.get_S_el()
    return Hsum + R*T - T*Ssum


